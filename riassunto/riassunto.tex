\documentclass[a4paper]{article}
\usepackage{parskip}
\usepackage{hyperref}
\usepackage{amsmath}
\usepackage{amsfonts}

\author{Luca Arnaboldi}
\title{Scientific Programming II\\ \emph{Support Vector Classifier for handwritten digits}}

\begin{document}
  \maketitle
  \section*{Objective of the program}
  We aim to implement a \emph{linear support vector classifier} that can distinguish between images of handwritten ``1'' and ``8''.
  
  Our program will take as input a set of already solved samples (\emph{training algorithm}),
  optimize the SVC parameters and evaluate the performance on previously unseen images(\emph{test set}).
  The algorithm used for the optimization will be the \emph{stochastic gradient descent}.

  We also implement a feature for saving the optimized parameters,
  in order to avoid the training every time the program is executed.

  \section*{Methodology}
  We assume that the images are grayscale, and each pixel is a value between 0(white) and 1(black).
  In our analysis we are not using the ``geometrical structure'' of the image; we map the image in a vector of dimension \(d\) equal to the number of pixels.

  \subsection*{Linear SVC}
  For details see \href{https://en.wikipedia.org/wiki/Support-vector_machine#Linear_SVM}{this web page}.

  Let's assume the train set is given by \(n\) already classified images 
  \[
    \left\{(\vec{x}_i,y_i)\right\}_{i=1,\dots n},
  \]
  where \(\vec{x}_i \in \mathbb{R}^d\) are the images' vectors, while \(y_i\) are the correspondings labels: 1 if the images contains a 1, or -1 if the images contains an 8.
  This peculiar choice of labels is for simplifying the following math.

  The linear support vector classifier is nothing more than ``a hyperplane that separates the inputs in their space''.
  Using the formulas, given an image \(\vec{x}\) as input, the SVC classifies in the following way:
  \[
    \begin{cases}
      \text{it's a 1}\qquad \text{if }\vec{w}\cdot\vec{x}-b\ge0 \\
      \text{it's a 8}\qquad \text{if }\vec{w}\cdot\vec{x}-b < 0 \\
    \end{cases},
  \]
  where \((\vec{w},b)\) are the \emph{parameters} of the SVC; changing the parameters the behaviour of the classifier changes,
  and in the end also its performance.

  \subsection*{Find the best parameters}
  We would like to find the best parameters (a.k.a. best plane) that describe our input as accurately as possible.
  Ideally, if the inputs were linearly separable, then we would look for a plane that separates them.
  This is not the case in any real setting, but still, we aim for a plane that separates as many inputs as possible.

  The \emph{hinge loss function} gives us a measure of the badness of the classifier on a given sample \((\vec{x}_i,y_i)\)
  \[
    \max\left(0,1-y_i(\vec{w}\cdot\vec{x}-b)\right).
  \]
  The error on the training set is given by the average on all samples
  \[
    \frac1n\sum_{i=1}^n \max\left(0,1-y_i(\vec{w}\cdot\vec{x}-b)\right).
  \]
  Finding the best parameters means minimize this expression varying \((\vec{w},b)\).

  As commonly do in machine learning, we add a \emph{regularization} term. 
  We finally get the \emph{loss function}
  \[
    \mathcal{L} = \frac12 \lambda |\vec{w}|^2 + \frac1n\sum_{i=1}^n \max\left(0,1-y_i(\vec{w}\cdot\vec{x}-b)\right)
  \],
  where \(\lambda\) is called \emph{regularization parameter}. This is the function our algorithm is going to minimize.

  \subsection*{Optimization algorithm}
  There are many choices we can do for finding the minimum of the loss function.

  We are going to use is a variant of the \emph{gradient descent}. GD is essentially the procedure that starts at a random point,
  and move toward the direction opposite to the gradient, until a stationary point is obtained.
  Mathematically, the updated rule we are going to use is
  \[
    (\vec{w},b)^{(\nu+1)} = (\vec{w},b)^{(\nu)} -\gamma \nabla_{\vec{w},b}\left(\mathcal{L}{\left((\vec{w},b)^{(\nu)}\right)}\right)
  \]
  \(\gamma\) is known as \emph{learning rate}, it regulates the granularity of the descent.

  Computing the gradient of \(\mathcal{L}\) at each step can be computationally heavy.
  Following the state of the art of machine learning, instead of computing the gradient of the full loss function,
  we use just one single input at each step. The input is chosen randomly in the training set.
  This correction is known as \emph{stochastic gradient descent} and drastically improves the performance of the algorithm.
  
  Moreover, the loss function is not convex, so a standard gradient descent is not guaranteed to find the global minimum.
  Using the SGD, we introduce some noise in the process that is observed preventing the descent to get stuck in a local stationary point.


  \section*{Input/Output}
  All the interactions with the programs are handled through files and command line arguments:
  \[
    \text{\texttt{digitclassifier [OPTIONS]}}
  \]
  These are the possible command line arguments:
  \begin{itemize}
    \item \texttt{--train <train file>} (or \texttt{-t})
          With this option, the optimization procedure is activated.
          It randomly initialize \(\vec{w}\) (using the dimension \(d\)) and \(b\), and than runs a SGD with \(S\) steps.
          The train set is also specified in the file.
          The exact format that the file can be seen below.
    \item \texttt{--save <save file>} (or \texttt{-s})
          Save the trained SVC parameters in a file to be loaded later.
          The exact format that the file can be seen below.
    \item \texttt{--load <train file>} (or \texttt{-l})
          Load a previously trained SVC.
          The exact format that the file can be seen below.
    \item \texttt{--test <train file>} (or \texttt{-T})
          Give a test set to evaluate the previously trained (or loaded) SVC.
          This option is available only if one between \texttt{--train} and \texttt{--load} is also passed.
          On the command line is printed the accuracy.
          The exact format that the file can be seen below.
          \item \texttt{--help} (or \texttt{-h}) Print command line instructions.
    \item \texttt{--image <width> <height> <image file>} (or \texttt{-i})
          Save a \texttt{.ppm} image cointating an heatmap of \(\vec{w}\).
          The vector is sliced in subvectors of lenghts \texttt{width} and then they are stacked (first on top) in column of lenght \texttt{height}.
          If \(\text{width}\times\text{height}\) is non equal to \(d\), an error is returned.
  \end{itemize}

  If \(d\) is not consistent between different files passed, an error is returned.
  If the files are not in the exact format indicated below, then the program is not expected to work.

  The dataset (train and test) used is \href{https://www.tensorflow.org/datasets/catalog/mnist}{MNIST}.
  It can be downloaded using a TensorFlow script.

  \section*{File formats}
  We use the same notation we introduced above. Here is a recap:
  \begin{itemize}
    \item $d$ \texttt{integer}: number of pixels in the images;
    \item $\gamma$ \texttt{real}: learning rate;
    \item $\lambda$ \texttt{real}: regularization parameter;
    \item $S$ \texttt{integer}: number of steps of the SGD;
    \item $n$ \texttt{integer}: size of the training set;
    \item $y_i$ \texttt{real[-1 or 1]}: label of an image;
    \item $x_{i,j}$ \texttt{real}: $j$th pixel of an image;
    \item $b$ \texttt{real}: bias of the machine;
    \item $w_{j}$ \texttt{real}: $j$th component of the machine vector;
    \item $m$ \texttt{integer}: size of the test set.
  \end{itemize}
  \subsection*{Train File}
  \begin{center}
    \begin{tabular}{ccccc}
      $d$ &&&&\\
      $\gamma$ & $\lambda$ & S&&\\
      $n$ &&&& \\
      $y_1$ & $x_{1,1}$ & $x_{1,2}$ & $\cdots$ & $x_{1,d}$ \\
      $y_2$ & $x_{2,1}$ & $x_{2,2}$ & $\cdots$ & $x_{2,d}$ \\
      $\vdots$ & $\vdots$ & $\vdots$ & $\ddots$ & $\vdots$ \\
      $y_n$ & $x_{n,1}$ & $x_{n,2}$ & $\cdots$ & $x_{n,d}$ \\
    \end{tabular}
  \end{center}
  \subsection*{Save/Load File}
  \begin{center}
    \begin{tabular}{ccccc}
      $d$ &&&&\\
      $b$ & $w_{1}$ & $w_{2}$ & $\cdots$ & $w_{d}$ \\
    \end{tabular}
  \end{center}
  \subsection*{Test File}
  \begin{center}
    \begin{tabular}{ccccc}
      $d$ &&&&\\
      $m$ &&&& \\
      $y_1$ & $x_{1,1}$ & $x_{1,2}$ & $\cdots$ & $x_{1,d}$ \\
      $y_2$ & $x_{2,1}$ & $x_{2,2}$ & $\cdots$ & $x_{2,d}$ \\
      $\vdots$ & $\vdots$ & $\vdots$ & $\ddots$ & $\vdots$ \\
      $y_n$ & $x_{m,1}$ & $x_{m,2}$ & $\cdots$ & $x_{m,d}$ \\
    \end{tabular}
  \end{center}

  \vfill
  \small
  As a final note, we point out that the program does not just produce a classifier for handwritten images, but could be used for a generic SVC.
  The important thing is that the format of the data complies with the instructions given above, then any dataset with \(\vec{x}\in\mathbb{R}^d\) and \(y\in\{-1,1\}\) can be used.
  Nevertheless, we are going to use only the MNIST dataset.

  It's possible to run \texttt{python~generate\_datasets.py} to obtain the train/test sets for all the possible pairs of digits.

  \newpage
  \section*{Computation of the gradients}
  The loss function of a single sample can be written as 
  \[
    \mathcal{L}_i = 
    \begin{cases}
      \frac12 \lambda |\vec{w}|^2 &\quad \text{if } y_i(\vec{w}\cdot\vec{x}_i-b) \ge 1.\\
      \frac12 \lambda |\vec{w}|^2 + 1-y_i(\vec{w}\cdot\vec{x}_i-b) &\quad \text{if } y_i(\vec{w}\cdot\vec{x}_i-b) < 1.
    \end{cases}.
  \]

  Let's compute the gradient
  \[
    \nabla_{w_j}\mathcal{L}_i = 
    \begin{cases}
      \lambda w_j &\quad \text{if } y_i(\vec{w}\cdot\vec{x}_i-b) \ge 1.\\
      \lambda w_j -y_i x_{i,j} &\quad \text{if } y_i(\vec{w}\cdot\vec{x}_i-b) < 1.
    \end{cases}.
  \]
  \[
    \nabla_{b}\mathcal{L}_i = 
    \begin{cases}
      0   &\quad \text{if } y_i(\vec{w}\cdot\vec{x}_i-b) \ge 1.\\
      y_i &\quad \text{if } y_i(\vec{w}\cdot\vec{x}_i-b) < 1.
    \end{cases}.
  \]
\end{document}