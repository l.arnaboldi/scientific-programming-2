module global_utilities
  implicit none
  ! Set kind to support values between -10^9 and 10^9 (excluded)
  integer, parameter :: int_kind = selected_int_kind(9)
  ! Set precision and range for real numbers
  integer, parameter :: real_kind = selected_real_kind(14,99)

  ! ESC char for the ANSI escape
  character(len=*), parameter :: ESC = achar(27)

  contains

  function randint(max) result(rnd)
    ! Generate a random integer in [1,max]
    integer(kind=int_kind), intent(in)  :: max
    integer(kind=int_kind)              :: rnd

    real :: u
    call random_number(u)

    rnd = 1 + floor(max*u)
  end function

  subroutine read_data(file_unit, d, n, x, y)
    integer, intent(in)                :: file_unit
    integer(kind=int_kind), intent(in) :: d, n
    real(kind=real_kind), intent(out)  :: x(:,:), y(:)

    integer(kind=int_kind) :: i, j
    real(kind=real_kind) :: data_line(d+1)

    do i = 1, n
      read(file_unit, *) data_line
      y(i) = data_line(1)
      ! TODO: consider to use a forall for the next loop
      do j = 2, d+1
        x(j-1,i) = data_line(j)
      end do
    end do
  end subroutine

  subroutine progress_bar(iteration, maximum)
    implicit none
    integer, intent(in) :: iteration, maximum

    integer             :: counter
    integer             :: step, done
    character(len=3)    :: last_advance

    step = nint(iteration * 100 / (1.0 * maximum))
    done = floor(step / 10.0)  ! mark every 10%

    ! This is the special ANSI ESCAPE for removing the previous printed progress bar (max width 36)
    write (*, '(a)', advance='no') ESC // '[36D' // ESC // '[K'

    write(*,'(a)',advance='no') ' -> In progress... ['
    if (done.le.0) then
      do counter = 1, 10
        write(*,'(a)',advance='no') '='
      end do
    else if ((done.gt.0).and.(done.lt.10)) then
      do counter = 1, done
        write(*,'(a)',advance='no') '#'
      end do
      do counter = done+1, 10
        write(*,'(a)',advance='no') '='
      end do
    else
      do counter = 1, 10
        write(*,'(a)',advance='no') '#'
      end do
    end if
    write(*,'(a)',advance='no') '] '
    write(*,'(I3.1)',advance='no') step

    if (iteration.eq.maximum) then
      last_advance = 'yes'
    else
      last_advance = 'no'
    end if
    write(*,'(a)',advance=last_advance) '%'
  end
end module global_utilities

module ppm_heatmap
  use global_utilities, only : real_kind
  implicit none
  ! Need integer that cointains [0,256)
  integer, parameter :: rgb_int = selected_int_kind(3)

  ! Just need the first 3 decimal digits, since we always care of int(255*real)
  ! Always between -1 and 1
  integer, parameter :: rgb_real = selected_real_kind(3,1)

  type :: RGB
    integer(kind=rgb_int) :: red
    integer(kind=rgb_int) :: green
    integer(kind=rgb_int) :: blue  
  end type

  contains

  pure function heatmap_color(value, abs_max_value) result(color)
    real(kind=real_kind), intent(in) :: value, abs_max_value

    real(kind=rgb_real) :: scale
    type(RGB) :: color

    scale = real(value / abs_max_value, kind=rgb_real) ! Initialiazing at declaration implies SAVE in Fortran08. That's why I can't compact in one single line

    if (scale.ge.0.) then
      color%red = int(255 * (1.-scale), rgb_int)
      color%green = int(255 * (1.-scale), rgb_int)
      color%blue = int(255, rgb_int)
    else
      scale = -scale
      color%red = int(255, rgb_int)
      color%green = int(255 * (1.-scale), rgb_int)
      color%blue = int(255 * (1.-scale), rgb_int)
    end if
  end function

  subroutine plot_heatmap(filename, width, height, w)
    character(len=32), intent(in)     :: filename
    integer, intent(in)               :: width, height
    real(kind=real_kind), intent(in)                  :: w(:)

    real(kind=real_kind)  :: abs_max_value = 0.
    integer               :: file_unit, ios, pixel
    type(RGB)             :: color

    do pixel = 1, width*height
      abs_max_value = max(abs_max_value, abs(w(pixel)))
    end do

    open(newunit = file_unit, iostat = ios,file=filename, action='write', decimal='point')

    write(file_unit, '(a)') 'P3'
    write(file_unit, '(i5,i5)') width, height
    write(file_unit, '(i3)')    255

    do pixel = 1, width*height
      color = heatmap_color(w(pixel), abs_max_value)
      write(file_unit, '(i3,a,i3,a,i3)') color%red, ' ', color%green, ' ', color%blue
    end do

    close(file_unit)
    
  end subroutine

end module ppm_heatmap


program digitclassifier
  use global_utilities
  use ppm_heatmap, only : plot_heatmap
  implicit none

  integer :: args_iterator, argc
  character(len=32) :: arg, arg_placeholder
  logical :: run_train = .false., run_save = .false., run_test = .false., run_load = .false., run_image = .false.
  character(len=32) :: train_file, save_file, test_file, load_file, image_file
  integer :: image_width, image_height

  integer(kind=int_kind)            :: d
  logical                           :: is_d_defined = .false.
  real(kind=real_kind), allocatable :: w(:)
  real(kind=real_kind)              :: b

  integer(kind=int_kind)            :: n
  real(kind=real_kind)              :: gamma, lambda
  integer(kind=int_kind)            :: S
  real(kind=real_kind), allocatable :: x_train(:,:), y_train(:)


  if (command_argument_count().eq.0) then
    call print_help()
  end if

  argc = command_argument_count()
  args_iterator = 1
  do while(args_iterator <= argc)
    call get_command_argument(args_iterator, arg)

    select case (arg)
    case ('-t', '--train')
      args_iterator = args_iterator + 1
      call get_command_argument(args_iterator, train_file)
      run_train = .true.
    case ('-s', '--save')
      args_iterator = args_iterator + 1
      call get_command_argument(args_iterator, save_file)
      run_save = .true.
    case ('-T', '--test')
      args_iterator = args_iterator + 1
      call get_command_argument(args_iterator, test_file)
      run_test = .true.
    case ('-l', '--load')
      args_iterator = args_iterator + 1
      call get_command_argument(args_iterator, load_file)
      run_load = .true.
    case ('-i', '--image')
      args_iterator = args_iterator + 1
      call get_command_argument(args_iterator, arg_placeholder)
      read(arg_placeholder, *) image_width

      args_iterator = args_iterator + 1
      call get_command_argument(args_iterator, arg_placeholder)
      read(arg_placeholder, *) image_height

      args_iterator = args_iterator + 1
      call get_command_argument(args_iterator, image_file)

      run_image = .true.
    case ('-h', '--help')
      call print_help()
    case default
      write(*, '(a,a,/)') 'Unrecognized command-line option: ', arg
      stop
    end select
    args_iterator = args_iterator + 1
  end do

  ! Check if the arguments are consistents
  if (run_save.and.(.not.run_train)) then
    write (*,'(a)') "Stopping! You can't specify --save without --train."
    stop
  end if

  if ((run_image.or.run_test).and.(.not.(run_train.or.run_load))) then
    write (*,'(a)') "Stopping! You can't run --test without --train nor --load."
    stop
  end if

  ! Load file
  if (run_load) then
    write (*,'(a)',advance='no') "Loading file... "
    call load_svc_from_file(load_file, d, b, w)
    is_d_defined = .true.
    write (*,'(a)') "Done!"
  end if

  ! Train file read
  if (run_train) then
    write (*,'(a)',advance='yes') "Running train... "
    call load_training_set_from_file(train_file, d, is_d_defined, gamma, lambda, S, n, x_train, y_train)
  end if

  ! Init parameters if not loaded
  if (.not.run_load) then
    b = 0.
    allocate(w(d))
    w = 0.
  end if

  ! Run Train
  if (run_train) then
    call stochastic_gradient_descent(d, b, w, gamma, lambda, n, x_train, y_train, S)
    deallocate(x_train, y_train)
  end if

  ! Save SVC
  if (run_save) then
    write (*,'(a)',advance='no') "Saving SVC... "
    call save_svc_to_file(save_file, d , b, w)
    write (*,'(a)') "Done!"
  end if

  ! Run Test
  if (run_test) then
    write (*,'(a)') "Running tests... "
    call test_svc_from_file(test_file, d, b, w)
    write (*,'(a)') "Done!"
  end if

  ! Plot Image
  if (run_image) then
    write (*,'(a)',advance='no') "Plotting Image... "
    if (image_width*image_height.ne.d) then
      write (*,'(a)') "\nStopping! Image dimension does not match with d!"
      stop
    end if
    call plot_heatmap(image_file, image_width, image_height, w)
    write (*,'(a)') "Done!"
  end if


contains

  subroutine print_help()
    write(*, '(a)')  'usage: digitclassifier [OPTIONS]'
    write(*, '(a)')  ''
    write(*, '(a)')  'Without further options, digitclassifier just does nothing.'
    write(*, '(a)')  ''
    write(*, '(a)')  'cmdline options:'
    write(*, '(a)')  ''
    write(*, '(a)')  '  -t, --train <filename>                  file containing the training set.'
    write(*, '(a)')  '  -T, --test <filename>                   file containing the test set'
    write(*, '(a)')  '  -s, --save <filename>                   file for saving the trained machine. Ignored if --train is not set'
    write(*, '(a)')  '  -l, --load <filename>                   file containing the trained machine.'
    write(*, '(a)')  '  -i, --image <filename> <width> <height> print information on usage of the program'
    write(*, '(a)')  '  -h, --help                              print information on usage of the program'
  end subroutine print_help

  subroutine load_svc_from_file(filename,d,b,w)
    use global_utilities
    implicit none

    character(len=32), intent(in)                   :: filename
    integer(kind=int_kind), intent(out)             :: d
    real(kind=real_kind), intent(out)               :: b
    real(kind=real_kind), intent(out), allocatable  :: w(:)

    integer :: file_unit, ios

    open(newunit = file_unit, iostat = ios,file=filename, status='old', action='read', decimal='point')
    read(file_unit, *) d
    allocate(w(d))
    read(file_unit, *) b, w
    close(file_unit)

  end subroutine

  subroutine load_training_set_from_file(filename, d, is_d_defined, gamma, lambda, S, n, x, y)
    use global_utilities
    implicit none

    character(len=32), intent(in)                   :: filename
    integer(kind=int_kind), intent(inout)           :: d
    logical, intent(inout)                          :: is_d_defined
    real(kind=real_kind), intent(out)               :: gamma, lambda
    integer(kind=int_kind), intent(out)             :: S
    integer(kind=int_kind), intent(out)             :: n
    real(kind=real_kind), intent(out), allocatable  :: x(:,:), y(:)

    integer :: file_unit, ios
    integer(kind=int_kind)            :: d_tmp

    open(newunit = file_unit, iostat = ios,file=filename, status='old', action='read', decimal='point')

    read(file_unit, *) d_tmp
    if (is_d_defined.and.(d_tmp .ne. d)) then
      write (*,'(a)') 'Error! d must be the same in --load and --train'
      stop
    end if
    d = d_tmp

    read(file_unit, *) gamma, lambda, S

    read(file_unit, *) n
    allocate(x_train(d,n), y_train(n))

    call read_data(file_unit, d, n, x, y)

    close(file_unit)
  end subroutine

  subroutine stochastic_gradient_descent(d, b, w, gamma, lambda, n, x, y, steps)
    use global_utilities
    implicit none

    integer(kind=int_kind), intent(in)  :: d, n, steps
    real(kind=real_kind), intent(inout) :: w(:), b
    real(kind=real_kind), intent(in)    :: gamma, lambda, x(:,:), y(:)

    integer(kind=int_kind) :: s, i, j
    real(kind=real_kind) :: svc_eval
    ! TODO: may be there is a more efficient implementation
    do s = 1, steps
      i = randint(n)
      svc_eval = 0.
      do j = 1, d
        svc_eval = svc_eval + w(j)*x(j,i)
      end do
      svc_eval = (svc_eval - b) * y(i)

      if (svc_eval.ge.1) then
        ! b is unchanged in this case
        do j = 1, d
          w(j) = w(j) - gamma * (lambda * w(j))
        end do
      else
        b = b - gamma * y(i)
        do j = 1, d
          w(j) = w(j) - gamma * (lambda * w(j) - y(i)*x(j,i))
        end do
      end if

      if ((mod(s,max(1,int(steps/100))).eq.0).or.(s.eq.steps)) then
        call progress_bar(s, steps)
      end if
    end do
  end subroutine

  subroutine save_svc_to_file(filename, d, b, w)
    use global_utilities
    implicit none

    character(len=32), intent(in)       :: filename
    integer(kind=int_kind), intent(in)  :: d
    real(kind=real_kind), intent(in)    :: w(:), b

    integer :: file_unit, ios

    open(newunit = file_unit, iostat = ios,file=filename, action='write', decimal='point')
    write(file_unit, '(i5)') d
    write(file_unit, *) b, w
    close(file_unit)
  end subroutine

  subroutine test_svc_from_file(filename, d, b, w)
    character(len=32), intent(in)       :: filename
    integer(kind=int_kind), intent(in)  :: d
    real(kind=real_kind), intent(in)    :: w(:), b

    integer                           :: file_unit, ios
    integer(kind=int_kind)            :: d_tmp
    integer(kind=int_kind)            :: m, correct_preditions = 0
    real(kind=real_kind), allocatable :: x(:,:), y(:)
    integer                           :: i, j
    real(kind=real_kind)              :: svc_eval, accuracy

    open(newunit = file_unit, iostat = ios,file=filename, status='old', action='read', decimal='point')

    read(file_unit, *) d_tmp
    if (d_tmp .ne. d) then
      write (*,'(a)') 'Error! d must be the same in --train(or --load) and --test'
      stop
    end if

    read(file_unit, *) m

    allocate(x(d,m),y(m))
    call read_data(file_unit, d, m, x, y)

    close(file_unit)

    do i = 1, m
      svc_eval = 0.
      do j = 1, d
        svc_eval = svc_eval + w(j)*x(j,i)
      end do
      !write(*,*) svc_eval, y(i), svc_eval*y(i)/sign(svc_eval*y(i), real(1., kind=real_kind))
      svc_eval = (svc_eval - b) * y(i)

      if (svc_eval.gt.0.) then
        correct_preditions = correct_preditions + 1
      end if
    end do
    deallocate(x, y)

    accuracy = 100. * real(correct_preditions, real_kind) / real(m, real_kind)

    write (*, '(a,i5,a,i5,a,f4.1,a)') 'Performance measure on test set: ', correct_preditions,'/',m,' (',accuracy,'%)'
  end subroutine
end program digitclassifier