import tensorflow_datasets as tfds
import numpy as np
from tqdm import tqdm

d = 28*28
learning_rate = 0.0001
regularization = 0.01
epochs = 100

mnist_data = tfds.load('mnist')
train, test = mnist_data['train'], mnist_data['test']

def example2myformat(example, plus_digit, minus_digit):
  image, digit_label = example['image'], example['label']
  image = image.reshape(((d)))/255
  if digit_label not in [plus_digit, minus_digit]:
    raise ValueError
  if digit_label == plus_digit:
    label = 1.
  else:
    label = -1.
  return str(label) + ' ' + ' '.join(map(lambda x: f'{x:.4f}', image))

def generate_2digits_traintest(plus_digit = 1, minus_digit = 8):
  test_list = []
  for example in tqdm(tfds.as_numpy(test)):
    try:
      test_list.append(example2myformat(example, plus_digit, minus_digit))
    except ValueError:
      pass

  train_list = []
  for example in tqdm(tfds.as_numpy(train)):
    try:
      train_list.append(example2myformat(example, plus_digit, minus_digit))
    except ValueError:
      pass

  with open(f'datasets/mnist{plus_digit}{minus_digit}-test.data', 'w') as write_test:
    write_test.write(f'{d}\n')
    write_test.write(f'{len(test_list)}\n')
    write_test.write('\n'.join(test_list))

  with open(f'datasets/mnist{plus_digit}{minus_digit}-train.data', 'w') as write_train:
    write_train.write(f'{d}\n')
    write_train.write(f'{learning_rate} {regularization} {epochs*len(train_list)}\n')
    write_train.write(f'{len(train_list)}\n')
    write_train.write('\n'.join(train_list))

if __name__ == '__main__':
  for pd in range(10):
    for md in range(pd+1, 10):
      print(f'Generating file for pair: {pd} {md}')
      generate_2digits_traintest(pd, md)