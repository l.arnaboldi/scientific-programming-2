# DigitClassifer
Per eseguire il progetto basta usare il comando:
```
make run
```

## Cifre diverse da 1-8
Bisogna innanzitutto generare il dataset completo con:
```
make generate_full_dataset
```
**Attenzione!** Il comando precedente:
 - Installa ```tensorflow```, ```numpy``` e altri pacchetti Python minori. È consigliato usarlo in un virtualenv.
 - Richiede 3.5GB di spazio
 - Sul mio PC richiede quasi 20 minuti.

Per testare il programma su una coppia di cifre diverse da 1,8 è suffciente:
```
make run DIGIT_PLUS=[cifra1] DIGIT_MINUS=[cifra2]
```
**NB:** deve valere ```cifra1 < cifra2```